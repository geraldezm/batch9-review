import Vue from "vue";
import Router from "vue-router";
import Album from "./components/Album.vue";

Vue.use(Router);

export default new Router({
  //mode: "history",
  //base: process.env.BASE_URL,
  routes: [
    {
      path: "*",
      redirect: "/album"
    },
    {
      path: "/album",
      name: "album",
      component: Album
    }
  ]
});
