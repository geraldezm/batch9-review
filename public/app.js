import AWS from "aws-sdk";
var albumBucketName = "photo-gallery-aws-project";
var bucketRegion = "us-west-2";
var IdentityPoolId = "us-west-2:6bf71262-6a01-4db2-bdd3-6400c8c12de6";

AWS.config.update({
  region: bucketRegion,
  credentials: new AWS.CognitoIdentityCredentials({
    IdentityPoolId: IdentityPoolId
  })
});

var s3 = new AWS.S3({
  apiVersion: "2006-03-01",
  params: { Bucket: albumBucketName }
});

function viewAlbum() {}

var exp = { s3, albumBucketName, viewAlbum };
export { exp };
